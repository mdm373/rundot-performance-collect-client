(function(require){
    "use strict";
    var spawn = require('child_process').spawnSync,
        config = require('./config.json'),
        fs = require('fs'),
        request = require('request'),
        $q = require("q"),
        readLine = require('readline'),
        diagnosticsData = { "version" : config.version, "clientName" : config.clientName},
        readFile = function(location){
            var deferred = $q.defer();
            fs.readFile(location, 'utf8', function(error, data){
                if(error){
                    deferred.reject(error)
                } else {
                    deferred.resolve(data);
                }
            });
            return deferred.promise;
        },
        parseXml = function(xmlString){
            var deferred = $q.defer();
            require('xml2js').parseString(xmlString, function(error, data){
                if(error){
                    deferred.reject(error);
                } else {
                    deferred.resolve(data);
                }
            });
            return deferred.promise;
        },
        populateDiagInfo = function(data){
            var diagInfo = { },
                dxDiagInfo = (data ? data.DxDiag : undefined),
                systemInformation = dxDiagInfo ? dxDiagInfo.SystemInformation[0] : {},
                displayDevices =  dxDiagInfo ? dxDiagInfo.DisplayDevices[0] : {},
                videoCard = displayDevices ? displayDevices.DisplayDevice[0] : {};
            diagInfo.processor = systemInformation.Processor ? systemInformation.Processor[0] : "unknown";
            diagInfo.memory = systemInformation.Memory ? systemInformation.Memory[0] : "unknown";
            diagInfo.videoCard = videoCard.CardName ? videoCard.CardName[0] : "unknown";
            diagnosticsData.dxDiagInfo = diagInfo;
            console.log("------so, your running this on something like the following:\n" + JSON.stringify(diagInfo) + "\n");
            return diagInfo;
        },
        populatePerfInfo = function(xmlData){
            var perfInfo = {};
            diagnosticsData.perfInfo = perfInfo;
            perfInfo.averageFrameMS = xmlData.PerfAnalysisMetrics.$.AverageFrameMS;
            perfInfo.adjustedAverageFrameMS = xmlData.PerfAnalysisMetrics.$.AdjustedAverageFrameMS;
            perfInfo.fastestFrameMS = xmlData.PerfAnalysisMetrics.$.FastestFrameMS;
            perfInfo.slowestFrameMS = xmlData.PerfAnalysisMetrics.$.SlowestFrameMS;
            perfInfo.testName = xmlData.PerfAnalysisMetrics.$.TestName;
            perfInfo.testDuration = xmlData.PerfAnalysisMetrics.$.TestDuration;
            console.log("------your machine cranked out these results:\n" + JSON.stringify(perfInfo) + "\n");
        },
        runPerformanceApp = function(){
            console.log("---running performance test...");
            spawn(config.perfApplication);
            console.log("------performance test ran!");
        },
        readPerfResults = function(){
            return readFile(config.perfResults);
        },
        performPost = function(url, data){
            var deferred = $q.defer(),
            postRequest = {
                json: data
            };
            request.post(url, postRequest, function(error, response, body){
                if(error) {
                    deferred.reject(error);
                } else if(response.statusCode != 200) {
                    deferred.reject(response);
                } else {
                    deferred.fulfill(response)
                }
            });
            return deferred.promise;
        },
        postPerfInfo = function(){
            console.log("---sharing all of this with " + config.reportAuthority);
            return performPost(config.reportUrl, diagnosticsData);
        },
        confirmPost = function(response){
            console.log("------and... done. You're the best, thanks for helping out with the test!");
        },
        handleError = function(error){
            console.error("\nZOMG something has gone terribly, terribly wrong! Please share this with " + config.reportAuthority);
            console.error("---Details: " + JSON.stringify(error));
            return {isErrorThrown:true};
        },
        prompt = function(message){
            var deferred = $q.defer(), lineReader = readLine.createInterface({input: process.stdin, output: process.stdout});
            lineReader.question(message, function(){
                lineReader.close();
                deferred.resolve();
            })
            return deferred.promise;
        },
        wrapThingsUp = function(errorDetails){
            if(errorDetails.isErrorThrown){
                return prompt("Press enter to exit.");
            }
            return true;
        };
    console.log(config.reportAuthority + " Performance Data Collector");
    console.log("WARNING: This application will share hardware information about this computer and it's performance results with " + config.reportAuthority + ".\n");

    console.log("---gathering details about your computer...");
    spawn('dxdiag', ['/64bit', '/x', 'dxdiag.xml']); //Win7 64 bit support
    console.log("---still gathering...");
    spawn('dxdiag', ['/x', 'dxdiag.xml']); //Win 8/10
    console.log("------computer information captured!");
    readFile("dxdiag.xml").then(parseXml).then(populateDiagInfo).then(runPerformanceApp).then(readPerfResults).then(parseXml).then(populatePerfInfo).then(postPerfInfo).then(confirmPost).catch(handleError).then(wrapThingsUp);


})(require);